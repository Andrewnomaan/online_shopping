<footer>
    <div class="container-fluid footer-top py-3">
      <div class="row">
        <div class="col-lg-3">
          <div class="footerheading">
            <h4>Policy</h4>
          </div>
          <ul>
            <li>
              <a href="">
                Privacy Policy
              </a>
            </li>
            <li>
              <a href="">
                Terms & Conditions
              </a>
            </li>
            <li>
              <a href="">
                Refund & Cancellation
              </a>
            </li>
            <li>
              <a href="">
                FAQ's
              </a>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <div class="footerheading">
            <h4>Get to Know Us</h4>
          </div>
          <ul>
            <li>
              <a href="">
                About Us
              </a>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <div class="footerheading">
            <h4>Connect with Us</h4>
          </div>
          <ul>
            <li>
              <a href="">
                Facebook
              </a>
            </li>
            <li>
              <a href="">
                Instagram
              </a>
            </li>
            <li>
              <a href="">
                Twitter
              </a>
            </li>
          </ul>
        </div>
        <div class="col-lg-3">
          <div class="footerheading">
            <h4>Contact Us</h4>
          </div>
          <ul>
            <li>
              <a href=""><span><i class="fas fa-map-marker-alt mr-2"></i>Bijnor, Uttar Pradesh,
                  India</span></a>
            </li>
            <li>
              <a href=""><span><i class="fas fa-phone-alt mr-2"></i>100-2444-444</span></a>
            </li>
            <li>
              <a href=""><span><i class="fas fa-envelope mr-2"></i>info@gmail.com</span></a>
            </li>
          </ul>
        </div>
      </div>
    </div>


    <div class="container-fluid footer-bottom py-3">
      <div class="row text-center">
        <div class="col-md-5 my-2">
          <span class="ml-2">
            &copy;Copyright 2021 adura.com, All Rights Reserved.
          </span>
        </div>

        <div class="col-md-4 my-2 developedby">
          <span>
            Developed by: Zoyo E-commerce Pvt. Ltd.
          </span>
        </div>

        <div class="col-md-3 my-2 footer-bottom-icon">
          <div class="mr-2">
            <a href=""><i class="fab fa-facebook"></i></a>
            <a href=""> <i class="fab fa-twitter"></i></a>
            <a href=""><i class="fab fa-instagram"></i></a>
            <a href=""><i class="fab fa-linkedin-in"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>