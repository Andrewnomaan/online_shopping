<?php
  include 'db_connect.php';
  $name_err="";
  $mobile_err="";
  $pin_err="";
  $address_err="";
  $er=0;
  if(isset($_POST['submit'])){
      $name=mysqli_real_escape_string($conn,$_POST['name']);
      $mobile=mysqli_real_escape_string($conn,$_POST['mobile']);
      $address=mysqli_real_escape_string($conn,$_POST['address']);
      $pin=mysqli_real_escape_string($conn,$_POST['pin']);
      $products=mysqli_real_escape_string($conn,$_POST['products']);
      $amount=mysqli_real_escape_string($conn,$_POST['amount']);
      if($name==""){
          $name_err="Plz enter your name";
          $er++;
      }
      else if(!preg_match("/^[a-zA-Z ]*$/",$name)){
        $name_err = "*Only letters and white space allowed";
        $er++;
    }
      if($mobile==""){
          $mobile_err="Plz enter your mobile no";
          $er++;
      }
      else if(strlen($mobile)!=10){
        $mobile_err="Plz enter valid mobile no";
        $er++;
      }
      if($address==""){
          $address_err="Enter your address";
      }
      else if(!preg_match("/^[0-9a-zA-Z,-]*$/",$name)){
        $address_err = "*Only alphabets and number are allowed";
         
      }
      if($pin==""){
          $address_err="Enter your pincode";
      }
      if($er==0){
        $name_err="";
        $mobile_err="";
        $sql="INSERT into orders values('','$name','$mobile','$address','$products','$amount','$pin')";
        $result=mysqli_query($conn,$sql);
        if($result){
            ?>
              <script>
                  alert("Your order placed successfully");
                  
              </script>
            <?php
            unset($_SESSION['cart']);
            ?>
            <script >
                window.location="index.php";
            </script>
            <?php
        }
      }
      
       
 }
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Adura Login</title>
    <link rel="stylesheet" href="css/loginstyle.css">
</head>

<body>
    <div class="container loginform">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="aduraloginhead py-3">
                    <h2>Order Form</h2>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="loginformdiv">
                    <form action="" name="signupform"  method="POST">
                        <div class="form-group">
                           
                        </div>
                        <div class="form-group">
                            <label for="email">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name">
                            <span class="" class="invalidalert" style="color:red;"><?php echo $name_err;?></span>
                        </div>
                        <div class="form-group">
                            <label for="email">Whatsapp No</label>
                            <input type="number" class="form-control" id="mobile" name="mobile" placeholder="Enter your Whatsapp no">
                            <span class="" class="invalidalert" style="color:red;"><?php echo $mobile_err;?></span>
                        </div>
                        <div class="form-group">
                            <label for="email">Address</label>
                            <input type="text" class="form-control" id="address" name="address" placeholder="Enter your address">
                            <span class="" class="invalidalert" style="color:red;"><?php echo $address_err;?></span>
                        </div>
                        <div class="form-group">
                            <label for="email">Pin Code:</label>
                            <input type="number" class="form-control" id="pincode" name="pin" placeholder="Enter your pincode">
                            <span class="" class="invalidalert" style="color:red;"><?php echo $pin_err;?></span>
                        </div>
                        <div class="form-group">
                            <label for="email">Your order</label>
                            <input type="text" class="form-control" id="mobile" name="products" value="<?php foreach($_SESSION['cart'] as $key=>$value){ echo $value ['name']." ".$value['qty'].",";}?>" readonly>
                          
                        </div>
                        <div class="form-group">
                            <label for="email">Total price</label>
                            <input type="text" class="form-control" name="amount" id="mobile" value="<?php echo $_SESSION['grand_total'];?>" readonly>
                          
                        </div>
                        <div class="form-group loginbutton mt-4">
                            <div class="row text-center">
                                <div class="col-md-12">
                                    <button type="submit" name="submit" class="btn myloginbtn" style="width:auto;">Order On Whatsapp</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>