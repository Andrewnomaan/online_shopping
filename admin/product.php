<?php
 require('top.php');
 if(isset($_GET['type']) && $_GET['type']!=''){
      $type=get_safe_value($conn,$_GET['type']);
     
      if($type=='delete'){
          $id=get_safe_value($conn,$_GET['id']);
          $delete_status="delete from products where id='$id'";
          $res_delete=mysqli_query($conn,$delete_status);
      }
 }
 $sql="SELECT products.*,categories.cat_name FROM products,categories where products.cat_id=categories.id order by products.id desc";
 $res=mysqli_query($conn,$sql);
?>
<div class="content pb-0">
            <div class="orders">
               <div class="row">
                  <div class="col-xl-12">
                     <div class="card">
                        <div class="card-body">
                           <h4 class="box-title">Products</h4>
                           <h4 class="box-link"><a href="manage_products.php">Add products</a></h4>
                        </div>
                        <div class="card-body--">
                           <div class="table-stats order-table ov-h">
                              <table class="table ">
                                 <thead>
                                    <tr>
                                       <th class="serial">S.No.</th>
                                       <th>ID</th>
                                       <th>Categories</th>
                                       <th>Name</th>
                                       <th>Image</th>
                                       <th>Price</th>
                                       <th>Qty</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                     <?php
                                     $i=1;
                                     while ($row=mysqli_fetch_assoc($res)) {
                                        echo'
                                        <tr>
                                       <td class="serial">'.$i++.'</td>
                                       <td class="serial">'.$row['id'].'</td>
                                       <td>'.$row['cat_name'].'</td>
                                       <td>'.$row['name'].'</td>
                                       <td><img src="../img/'.$row['img'].'"></td>
                                       <td>'.$row['price'].'</td>
                                       <td>'.$row['qty'].'</td>
                                       <td>';
                                       // if($row['status']==0){
                                       //     echo '<span class="badge badge-pending"><a href="?type=status&operation=active&id='.$row['id'].'">Deactive</a></span>&nbsp;&nbsp;';
                                       // }
                                       // else{
                                       //  echo '<span class="badge badge-complete"><a href="?type=status&operation=deactive&id='.$row['id'].'">Active</a></span>&nbsp;&nbsp;';
                                       // }
                                       echo'&nbsp;<span class="badge badge-edit"><a href="manage_products.php?id='.$row['id'].'">Edit</a></span>&nbsp;';
                                       echo'&nbsp;<span class="badge badge-delete"><a href="?type=delete&id='.$row['id'].'">Delete</a></span>';
                                       //
                                      echo' </td>
                                    </tr>';
                                     }
                                     ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
		  </div>
<?php
 require('footer.php');
?>          