<?php
 require('top.php');
 $categories_id='';
 $name='';
 $price='';
 $qty='';
 $image='';
 $filename='';
 $description='';
 $msg='';
 $id='';
 $img='';
 $image_required='required';
 if(isset($_GET['id']) && $_GET['id']!=''){
   $image_required='';
   $id=get_safe_value($conn,$_GET['id']);
   $res=mysqli_query($conn,"select * from products where id='$id'");
   $check=mysqli_num_rows($res);
   if($check>0){
   $row=mysqli_fetch_assoc($res);
   $categories_id=$row['cat_id'];
    $name=$row['name'];
    $price=$row['price'];
    $qty=$row['qty'];
    $description=$row['description'];
    
   }
   else{
      header("Location:product.php");
      die(); 
   }
}
 if(isset($_POST['submit'])){
    $categories_id=get_safe_value($conn,$_POST['categories_id']);
    $name=get_safe_value($conn,$_POST['name']);
    $price=get_safe_value($conn,$_POST['price']);
    $qty=get_safe_value($conn,$_POST['qty']);
    $description=get_safe_value($conn,$_POST['desc']);
    $image=$_FILES['file'];
    $file_tmp=$image['tmp_name'];
      $filename=$image['name'];
      $fileext=explode('.',$filename);
      $filecheck=strtolower(end($fileext));
      $filestored=array('png','jpg','jpeg');
    $res1=mysqli_query($conn,"select * from products where name='$name'");
    $check=mysqli_num_rows($res1);
    if($categories_id==0){
       $msg="Select your categories first";
    }
    if($filename!=''){
    if(!in_array($filecheck,$filestored)){
       $msg="Only images are allowed";
    }   
  } 
    if($check>0){
      if(isset($_GET['id']) && $_GET['id']!=''){
           $getData=mysqli_fetch_assoc($res1);
           if($id==$getData['id']){

           }
           else{
            $msg="Product already exist!";
           }
      }
      else{
        $msg="Product already exist!";
      }
    }
    if($msg==''){
    if(isset($_GET['id']) && $_GET['id']!=''){
       if($filename!=''){
      $destinationfile='../img/'.$filename;
      move_uploaded_file($file_tmp,$destinationfile);
      $sql=mysqli_query($conn,"update products set cat_id='$categories_id',name='$name',
      price='$price',qty='$qty',img='$filename',description='$description' where id='$id'");
    }
    else{
      $sql=mysqli_query($conn,"update products set cat_id='$categories_id',name='$name',
      price='$price',qty='$qty',description='$description' where id='$id'");
    }
   }
    else{
    $sql=mysqli_query($conn,"insert into products(name,cat_id,qty,price,img,description) values('$name','$categories_id','$qty','$price','$filename','$description')");
    
    }
    header("Location:product.php");
    die(); 
   }
 
   

  
}

?>
  <div class="content pb-0">
            <div class="animated fadeIn">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="card">
                        <div class="card-header"><strong>Add Product</strong></div>
                        <div class="card-body card-block">
                           <form action="" method="post" enctype="multipart/form-data">
                           <div class="form-group">
                           <label for="categories" class=" form-control-label">Categories</label>
                           <select name="categories_id" class="form-control">
                               <option>Select Categories</option>
                                <?php
                                $res=mysqli_query($conn,"select id,cat_name from categories");
                                   while($row=mysqli_fetch_assoc($res)){
                                      if($row['id']==$categories_id){
                                       echo "<option selected value=".$row['id'].">".$row['cat_name']."</option>";
                                      }
                                      else{
                                       echo "<option value=".$row['id'].">".$row['cat_name']."</option>";
                                      }
                                   }
                                ?>
                           </select>
                            </div>
                           <div class="form-group">
                               <label for="categories" class=" form-control-label">Products Name</label>
                           <input type="text" id="name" name="name" placeholder="Enter product name" class="form-control" required value="<?php echo $name; ?>">
                        </div>
                           <div class="form-group">
                               <label for="categories" class=" form-control-label">Price</label>
                           <input type="text" id="price" name="price" placeholder="Enter product price" class="form-control" required value="<?php echo $price; ?>">
                        </div>
                           <div class="form-group">
                               <label for="categories" class=" form-control-label">Qty</label>
                           <input type="text" id="qty" name="qty" placeholder="Enter quantity" class="form-control" value="<?php echo $qty; ?>" required>
                        </div>
                        <div class="form-group">
                               <label for="categories" class=" form-control-label">Image</label>
                           <input type="file" id="img" name="file" class="form-control" <?php echo $image_required ?>>
                        </div>
                         
                           <div class="form-group">
                               <label for="short_desc" class=" form-control-label">Description</label>
                           <textarea type="text" id="desc" placeholder="Enter product description" name="desc" class="form-control" required><?php echo $description; ?></textarea>
                        </div>
                         
                           <button id="cat-button" type="submit" class="btn btn-lg btn-info btn-block" name="submit">
                           <span id="payment-button-amount">Submit</span>
                           </button>
                           <div class="field_error"><?php
                             echo $msg;
                           ?></div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
<?php
 require('footer.php');
?>          