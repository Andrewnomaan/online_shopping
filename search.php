
 <?php 
  include 'modal.php';
  include('headerfull.php') ;
     $search='';
     if(isset($_GET['search'])){
       $search=mysqli_real_escape_string($conn,$_GET['search']);
    }
    $sql="SELECT * FROM products where products.name like'%$search%'";
      $res=mysqli_query($conn,$sql);

      //If item is present
      if(mysqli_num_rows($res)>0){
  ?>

<body>
  <!-- header  -->
 
  <!-- header end -->

  <!-- main content -->
  <!-- <section class="mb-2">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
                <div class="carousel-item active ">
                    <img class="d-block w-100 img-fluid" src="img/img3.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-fluid" src="img/img2.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-fluid" src="img/img4.jpg" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-fluid" src="img/img1.jpg" alt="Third slide">
                </div>
            </div>
      <a class="carousel-control-prev bg-secondary" style="width: 60px;" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next bg-secondary" style="width: 60px;" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </section> -->

  <!-- cards section -->
  <section class="my-5">
    <div class="container-fluid card-section">
      <div class="row mt-3 mb-5">
        <div class="col-md-12 text-center">
          <!-- <div class="section-title">
            <h2><strong><?php echo ucfirst($row1['cat_name']);?> Items</strong></h2>
          </div> -->
        </div>
      </div>
      <div id="message"></div>
      <div class="row">
      <?php
      
      
    while($row=mysqli_fetch_assoc($res))
    {
?>
          <div class="col-md-3 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <form class="form">
                                <span style="color:red;" id="<?php echo $row['id'];?>"></span>
                                <input type="hidden" class="pid" name="pid" value="<?php echo $row['id'];?>">
                                <input type="hidden" class="pname" name="p_name" value="<?php echo $row['name'];?>">
                                <input type="hidden" class="pprice" name="p_price" value="<?php echo $row['price'];?>">
                                <input type="hidden" class="pimage" name="p_image" value="<?php echo $row['img'];?>">
                                <input type="hidden" class="pcode" name="p_code"
                                    value="<?php echo $row['product_code'];?>">
                                <img src="img/<?php echo $row['img']; ?>" class="img-fluid" alt="img-not-found">
                                <div class="card-title text-center mt-2">
                                    <h4><?php echo $row['name']; ?></h4>
                                </div>
                                <div class="row mt-4">
                                <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="button" value="-" class="button-minus" data-field="quantity" style="min-width:34px;">
                                            <input type="number" step="1" max="" value="1" name="quantity" class="quantity-field quantity">
                                            <input type="button" value="+" class="button-plus" data-field="quantity" style="min-width:34px;">
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" name="add_cart" class="add_item"><i
                                                    id="<?php echo $row['name'].$row['id']?>"
                                                    class="far fa-heart mr-1"></i>Add to Cart</button>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                          <div class="form-group">
                                          <?php
                                                  if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true){
                                                ?>
                                                <button type="button" class="btn btn-info buy_now" data-toggle="modal" data-target="#myModal"><i class="fas fa-shopping-basket"></i>&nbsp;&nbsp;Order Now</button>
                                                <?php
                                                  }
                                                  else{
                                                ?>
                                                    <a href="signin.php" class="btn btn-info buy_now"><i class="fas fa-shopping-basket"></i>&nbsp;&nbsp;Order Now</a>
                                                <?php
                                                  }
                                                ?>
                                          </div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
                   }
                ?>
   
      </div>


  </section>
  <!-- main content end -->


  <!-- end section -->

  <!-- footer start -->
  <?php include('footer.php') ?>
  <!-- footer end -->

  <script>
    // Set the date we're counting down to
    var countDownDate = new Date("Nov 1, 2021 15:37:25").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Output the result in an element with id="demo"
      document.getElementById("timer").innerHTML = days + "d:" + hours + "h:" +
        minutes + "m:" + seconds + "s";

      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("timer").innerHTML = "EXPIRED";
      }
    }, 1000);
  </script>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
 
    <script>
    $(document).ready(function(){
        $('.add_item').click(function(e){
          e.preventDefault();
          var form=$(this).closest('.form');
          var pid=form.find('.pid').val();
          var pname=form.find('.pname').val();
          var pprice=form.find('.pprice').val();
          var pcode = form.find(".pcode").val();
          var pquantity = form.find(".quantity").val();
            var pimage = form.find(".pimage").val();
            var id = pname + pid;
            console.log(pname)
            console.log(id);
            $.ajax({
          url:'action.php',
          method:'post',
          data: {
                    pid: pid,
                    pname: pname,
                    pprice: pprice,
                    pimage: pimage,
                    pcode: pcode,
                    pquantity: pquantity
                },
          success:function(response){
            $('#message').html(response);
            $('#message').html(response);
                    // $(`#${id}`).removeClass('far');
                    // $(`#${id}`).addClass('fas');
                    // console.log(id);
                    load_cart_item();
          }
        })
        })
        load_cart_item();
        function load_cart_item(){
        $.ajax({
          url:'action.php',
          method:'get',
          data:{cartItem:"cart_item"},
          success:function(response){
              $('#count').html(response);
          }
        })
      }
      function incrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal)) {
                parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
            
        }

        function decrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal) && currentVal > 0) {
                parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
        }

        $('.input-group').on('click', '.button-plus', function(e) {
            incrementValue(e);
        });

        $('.input-group').on('click', '.button-minus', function(e) {
            decrementValue(e);
        });
      })
    </script>
     <?php
      }

      //If item is not present
     else{
         ?>

                 <div class="col-md-3 mb-4">
                                <div class="card-title text-center mt-2">
                                    <h4>No item found</h4>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                       
                                    </div>
                                        <div class="col-md-6">
                                            
                                        </div>
                                    </div>
                           
                        </div>
                 </div>
  <?php
  ?>
   <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  <script>
        $(document).ready(function(){
            function load_cart_item(){
        $.ajax({
          url:'action.php',
          method:'get',
          data:{cartItem:"cart_item"},
          success:function(response){
              $('#count').html(response);
          }
        })
      }
      load_cart_item();
        })
  </script>
  <?php
      }
  ?>
</body>

</html>