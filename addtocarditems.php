<?php
$grand_total=0;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zoyo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

    <?php include 'modal.php' ?>
    <!-- header  -->
    <?php include('headerfull.php') ?>
    <!-- header end -->

    <!-- main content -->

    <!-- cards section -->
    <section class="my-5">
        <div class="container-fluid addtocart-section">
            <div class="row mt-3 mb-5">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>My Cart</h2>
                    </div>
                    <div id="message"></div>
                </div>
            </div>
            <div style="display:<?php if(isset($_SESSION['showalert'])){ echo $_SESSION['showalert'];}else{echo 'none';}unset($_SESSION['showalert'])?>" class="alert alert-success alert-dismissible" role="alert">
         <strong><?php if(isset($_SESSION['message'])){ echo $_SESSION['message'];}unset($_SESSION['message'])?></strong>
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
            <div class="row justify-content-center">
           
                <div class="col-md-10 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Image</th>
                                            <th scope="col">Item Name</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Total Price</th>
                                            <th scope="col"><a href="action.php?clear=all" class="badge-danger badge p-1" 
                                            onclick="return confirm('Are you sure to want to clear your cart')"><i class="far fa-trash mr-1"></i>&nbsp;&nbsp;Clear your cart</a></th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php	
                                        if(isset($_SESSION['cart'])){
                                         foreach($_SESSION['cart'] as $key=>$row)
                                         {        
                                     ?>
                                        <tr>
                                        
                                            <th><?php echo $row["id"];?></th>
                                            <input type="hidden" class="pid" value="<?php echo $row['id'];?>">
                                            <input type="hidden" class="pimage" value="<?php echo $row['image'];?>">
                                            <th><img src="img/<?php echo $row["image"];?>" class="img" width="40"></th>
                                            <td><?php echo $row["name"];?></td>
                                            <td><i class="fas fa-rupee-sign"></i>&nbsp;&nbsp;<?php echo $row['price'];?></td>
                                           <input type="hidden" class="pprice" value="<?php echo $row['price'];?>">
                                            <td style="text-align:center;"> <div class="input-group">
                                            <input type="button" value="-" class="button-minus itemQtyminus" data-field="quantity" style="margin-left:20px;">
                                            <input type="number" step="1" max="" value="<?php echo $row['qty'];?>" name="quantity"
                                                class="quantity-field qty" style="width:85px;text-align:center; ">
                                            <input type="button" value="+" class="button-plus itemQty" data-field="quantity">
                                        </div></td>
                                            <td><i class="fas fa-rupee-sign"></i>&nbsp;&nbsp;<?php 
                                            echo $row['total_price'];
                                            ?></td>
                                            <td>
                                                <div class="form-group">
                                                <a href="action.php?remove=<?php echo $row['id'];?>" class="text-danger p-1" 
                                            onclick="return confirm('Are you sure to want to delete this item in your cart')"><i class="far fa-trash mr-1"></i>&nbsp;&nbsp;Remove from cart</a>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <a href=""><i class="fas fa-shopping-basket mr-1"></a>
                                                </div> -->
                                               
                                            </td>
                                            
                                           
                                        </tr>
                                        
                                        <?php
                                            $grand_total+=$row['total_price'];
                                            $_SESSION['grand_total']=$grand_total;
                                                }
                                            }
                                            ?>
                                            <tr>
                                            <td colspan="5" style="text-align:right;">
                                                <b >Grand Total:</b></t>
                                               <td><b><i class="fas fa-rupee-sign"></i>&nbsp;&nbsp;
                                               <?php 
                                               echo $grand_total;
                                               ?>
                                            </b> 
                                            </td>
                                            <td>
                                                <?php
                                                  if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true){
                                                ?>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fas fa-shopping-basket"></i>&nbsp;&nbsp;Order Now</button>
                                                <?php
                                                  }
                                                  else{
                                                ?>
                                                    <a href="signin.php" class="btn btn-info"><i class="fas fa-shopping-basket"></i>&nbsp;&nbsp;Order Now</a>
                                                <?php
                                                  }
                                                ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row mt-4 justify-content-between text-center">
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-3">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- main content end -->


    <!-- end section -->

    <!-- footer start -->
    <?php include('footer.php') ?>
    <!-- footer end -->

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script>
    $(document).ready(function(){

        //for change quantity of cart item
        $('.itemQty').on('click',function(){
            var $el=$(this).closest('tr');
            var pid=$el.find(".pid").val();
            var pprice=$el.find(".pprice").val();
            var itemQty=parseInt($el.find(".qty").val())+1;
            var img=$el.find(".pimage").val();
            location.reload(true);
            $.ajax({
          url:'action.php',
          method:'post',
          data:{pid2:pid,pprice:pprice,itemQty:itemQty,img:img,pcode:pcode},
          success:function(response){
              $('#message').html(response);
          } 
        })
        })
        $('.itemQtyminus').on('click',function(){
            var $el=$(this).closest('tr');
            var pid=$el.find(".pid").val();
            var pprice=$el.find(".pprice").val();
            var itemQty=parseInt($el.find(".qty").val())-1;
            var img=$el.find(".pimage").val();
            location.reload(true);
            $.ajax({
          url:'action.php',
          method:'post',
          data:{pid2:pid,pprice:pprice,itemQty:itemQty,img:img,pcode:pcode},
          success:function(response){
              $('#message').html(response);
          } 
        })
        })
        $('.qty').on('change',function(){
            var $el=$(this).closest('tr');
            var pid=$el.find(".pid").val();
            var pprice=$el.find(".pprice").val();
            var itemQty=parseInt($el.find(".qty").val());
            var img=$el.find(".pimage").val();
            location.reload(true);
            $.ajax({
          url:'action.php',
          method:'post',
          data:{pid2:pid,pprice:pprice,itemQty:itemQty,img:img},
          success:function(response){
              $('#message').html(response);
          } 
        })
        })
        load_cart_item();
        function load_cart_item(){
        $.ajax({
          url:'action.php',
          method:'get',
          data:{cartItem:"cart_item"},
          success:function(response){
              $('#count').html(response);
          }
        })
      }
      function incrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal)) {
                parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
            
        }

        function decrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal) && currentVal > 0) {
                parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
        }

        $('.input-group').on('click', '.button-plus', function(e) {
            incrementValue(e);
        });

        $('.input-group').on('click', '.button-minus', function(e) {
            decrementValue(e);
        });
    })
    </script>
</body>

</html>