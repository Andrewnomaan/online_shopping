
  <nav class="navbar topnavbar navbar-expand-lg py-4">
    <a class="navbar-brand" href="#">
      <h3>Zoyo</h3>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span><i class="fas fa-bars text-light"></i></span>
    </button>
    <form class="my-2 col-md-5">
      <input class="form-control" type="search" placeholder="Search" aria-label="Search">
    </form>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fas fa-mobile-alt mr-1"></i>Download App</a>
        </li>
        <div style="background-color: rgb(128, 128, 128); width: 2px;"></div>
        <li class="nav-item">
          <a class="nav-link" href="#">Become a Supplier</a>
        </li>
        <div style="background-color: rgb(128, 128, 128); width: 2px;"></div>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-mobile-alt mr-1"></i>Profile
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <div class="p-2">
              <h5 style="font-weight: 600;">Hello User</h5>
              <hr style="margin: 0px;">
              <span style="font-size: 12px; font-weight: 600; margin-top: 0px;">To access your account</span>
              <div class="text-center">
                <a href="" class="btn btn-warning mt-2">SignIn</a>
              </div>
              <hr style="margin-top: 8px; margin-bottom: 8px;">
              <div class="text-center">
                <span><span style="font-size: 12px;">New Customer?</span><a href="" class="ml-2">SignUp</a></span>
              </div>
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="far fa-shopping-cart mr-1"></i>Cart</a>
        </li>
      </ul>
    </div>
  </nav>
