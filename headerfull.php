<?php
  include 'db_connect.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Zoyo</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
  <link rel="stylesheet" href="css/style.css">
</head>
  <nav class="navbar topnavbar navbar-expand-lg py-4">
    <a class="navbar-brand" href="index.php">
      <h3>Adura</h3>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span><i class="fas fa-bars text-light"></i></span>
    </button>
    <form class="my-2 col-md-5" action="search.php" method="get">
      <input class="form-control" type="search" placeholder="Search" name="search" aria-label="Search">
    </form>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="#"><i class="fas fa-mobile-alt mr-1"></i>Download App</a>
        </li>
        <div style="background-color: rgb(128, 128, 128); width: 2px;"></div>
        <li class="nav-item">
          <a class="nav-link" href="#">Become a Supplier</a>
        </li>
        <div style="background-color: rgb(128, 128, 128); width: 2px;"></div>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-mobile-alt mr-1"></i>Profile
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <div class="p-2">
              <?php
                if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true){
              ?>
               <h5 style="font-weight: 600;">Hello <?php echo ucfirst($_SESSION['name']);?></h5>
               <hr style="margin-top: 8px; margin-bottom: 8px;">
              <div class="text-center">
                <a href="logout.php" class="btn btn-warning mt-2">Log out</a>
              </div>
               <?php
                }
                else{
               ?>
              <h5 style="font-weight: 600;">Hello User</h5>
              <hr style="margin: 0px;">
              <span style="font-size: 12px; font-weight: 600; margin-top: 0px;">To access your account</span>
              <div class="user_login text-center">
                <a href="admin/login.php?admin" class="">Admin login</a>
              <!-- <hr style="margin-top: 8px; margin-bottom: 8px;"> -->
                 <br>
                <a href="signin.php" class="">Sign in</a>
                 <br>
                <a href="signup.php" class="">Sign up</a>
              
              </div>
              <?php
                }
              ?>
              <!-- <div class="text-center">
                <span><span style="font-size: 12px;">New Customer?</span><a href="" class="ml-2">SignUp</a></span>
              </div> -->
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="addtocarditems.php"><i class="far fa-shopping-cart mr-1"></i>Cart<span id="count"></span></a>
        </li>
      </ul>
    </div>
  </nav>


  <nav class="navbar listnavbar navbar-expand-lg pt-1 pb-4">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
      aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="text-light">Menu</span>

    </button>
    <div class="collapse navbar-collapse mt-3" id="navbarText">
      <ul class="navbar-nav mr-auto">
        <?php
         $sql="SELECT * FROM categories";
         $res=mysqli_query($conn,$sql);
         while($row=mysqli_fetch_assoc($res)){
        ?>
        <li class="nav-item">
          <a href="categories.php?id=<?php echo $row['id'];?>&name=<?php echo $row['cat_name'];?>" class="nav-link"><?php echo ucfirst($row['cat_name']);?></a>
        </li>
        <?php
         }
        ?>
      </ul>
    </div>
  </nav>
  <!-- navbar end -->